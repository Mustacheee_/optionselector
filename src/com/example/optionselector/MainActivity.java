package com.example.optionselector;

import java.util.Random;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		Button submit;
		EditText enterNumber;
		TextView console;
		int number;
		int mode;
		SharedPreferences preferences;

		private static final int MODE_START = 0;
		private static final int MODE_SUBMIT = 1;

		OnClickListener clickSubmit = new OnClickListener(){
			@Override
			public void onClick(View v){
				submit();
			}
		};

		OnClickListener clickSelect = new OnClickListener(){
			@Override
			public void onClick(View v){
				selectOption();
			}
		};

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);

			this.preferences = this.getActivity().getPreferences(Context.MODE_PRIVATE);
			this.mode = preferences.getInt("mode", MODE_START);
			this.submit= (Button) rootView.findViewById(R.id.submitNumber);
			this.enterNumber = (EditText) rootView.findViewById(R.id.enterNumber);
			this.console = (TextView) rootView.findViewById(R.id.console);

			enterNumber.setText(preferences.getString("enterNumber", ""));
			console.setText(preferences.getString("console", ""));

			if(mode == MODE_SUBMIT){
				number = preferences.getInt("number", 1);
				submit.setOnClickListener(clickSelect);
				submit.setText("Okay");
			}
			else{
				submit.setOnClickListener(clickSubmit);
			}
			return rootView;
		}

		public void submit(){
			try{
				number = Integer.parseInt(enterNumber.getText().toString());
				if(number <= 0){
					console.setText("");
					console.setText("Please enter a valid number of options");
					enterNumber.setText("");
					return;
				}
			}
			catch(NumberFormatException e){
				console.setText("");
				console.setText("-____-\n" +
						"Really? Enter a realistic number of options");
				enterNumber.setText("");
				number = 0;
				return;
			}
			this.mode = MODE_SUBMIT;
			submit.setText("Okay");
			console.setText("Assign everyone a number and click Okay");
			submit.setOnClickListener(clickSelect);
		}

		public void selectOption(){
			Random r = new Random();
			int selected = r.nextInt(number);
			selected++;
			console.setText("");
			console.setText("Go with option: " + selected);
			submit.setOnClickListener(clickSubmit);
			submit.setText("Submit");
			this.mode = MODE_START;
		}

		@Override
		public void onPause(){
			super.onPause();
			Editor editor = preferences.edit();
			editor.putInt("mode", mode);
			editor.putInt("number", number);
			editor.putString("enterNumber", enterNumber.getText().toString());
			editor.putString("console", console.getText().toString());
			editor.commit();
		}
	}
}
